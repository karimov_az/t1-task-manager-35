package ru.t1.karimov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.api.model.IWBS;
import ru.t1.karimov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    public Task(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status,
            @Nullable final String projectId
    ) {
        super(userId);
        this.name = name;
        this.description = description;
        this.status = status;
        this.projectId = projectId;
    }

}
