package ru.t1.karimov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.response.AbstractUserResponse;
import ru.t1.karimov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull final User user) {
        super(user);
    }

}
