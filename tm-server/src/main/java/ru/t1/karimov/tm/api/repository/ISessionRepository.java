package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
